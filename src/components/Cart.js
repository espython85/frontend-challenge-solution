import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  addQuantity,
  removeItem,
  subtractQuantity,
} from "../redux/actions/cartActions";

export const Cart = (props) => {
  const { items, removeItem, addQuantity, subtractQuantity } = props;
  // assume shipping price is $10
  const shippingPrice = 10;
  let totalPrice = 0;
  let noItems = 0

  for (const item of items) {
    totalPrice =
      totalPrice + item.quantity * item.price + item.quantity * shippingPrice;
  }

  for (const item of items) {
    noItems =
    noItems + item.quantity;
  }

  const handleRemove = (id) => {
    removeItem(id);
  };

  const handleAddQuantity = (id) => {
    addQuantity(id);
  };

  const handleSubtractQuantity = (id) => {
    subtractQuantity(id);
  };

  const addedItems = items?.length ? (
    items.map((item) => {
      // Implement the cart list here
      return (
        <div key={item.id} className="row ">
          <div className="card horizontal col s12 cart-item">
            <div className="card-image">
              <img
                src={item.img}
                alt={item.title}
                width="200px"
                height="200px"
              />
            </div>
            <div className="card-stacked">
              <div className="card-content">
                <p className="text-center">{item.desc}</p>
              </div>
              <div className="card-action">
                <div className="cart-header">
                  <div className="">
                    <p className="text-center">
                      <b>
                        Price:
                        {item.price}$
                      </b>
                    </p>
                  </div>
                  <div className="">
                    <div className="cart-header">
                      <a className="btn-floating btn-small waves-effect waves-light blue-grey lighten-2" onClick={() => handleSubtractQuantity(item.id)}>
                        <i className="material-icons">remove</i>
                      </a>

                      <p className="quant">{item.quantity}</p>

                      <a
                        className="btn-floating btn-small waves-effect waves-light blue-grey lighten-2"
                        onClick={() => handleAddQuantity(item.id)}
                      >
                        <i className="material-icons">add</i>
                      </a>
                    </div>
                  </div>
                  <div className=" btn-container">
                    <a
                      className="waves-effect waves-light btn red"
                      onClick={() => handleRemove(item.id)}
                    >
                      <i className="material-icons right">delete</i>
                      Delete
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    })
  ) : (
    <p>Nothing.</p>
  );
  return (
    <div className="container">
      <div className="cart">
        <h5>You have ordered:</h5>
        {items && items.length > 0 && (
          <div className="cart-header">
            <h5>No of items: {noItems}</h5>
            <h5>Total price: ${totalPrice}</h5>
          </div>
        )}
        {addedItems}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => state.cart;

const mapDispatchToProps = (dispatch) => ({
  removeItem: (id) => {
    dispatch(removeItem(id));
  },
  addQuantity: (id) => {
    dispatch(addQuantity(id));
  },
  subtractQuantity: (id) => {
    dispatch(subtractQuantity(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
