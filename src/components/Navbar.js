import React from "react";
import { useSelector } from 'react-redux'

import { Link } from "react-router-dom";

const Navbar = () => {
  const items = useSelector(state=>state.cart.items)
  console.log('items',items)


  let noItems = 0

  for (const item of items) {
    noItems =
    noItems + item.quantity;
  }
  return (
    <nav className="nav-wrapper">
      <div className="container">
        <Link to="/" className="brand-logo">
          Shopping
        </Link>

        <ul className="right">
          <li>
            <Link to="/">Shop</Link>
          </li>
          <li>
            <Link to="/cart">My cart</Link>
          </li>
          <li>
            <Link to="/cart">
              <div className="cart-header">
              <i className="material-icons">shopping_cart</i>
              <span className="new badge blue-grey darken-2 nav-badge" data-badge-caption="">{noItems}</span>

              </div>
            </Link>
            
          </li>
         
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
