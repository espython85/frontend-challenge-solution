import React from 'react'
import { connect } from 'react-redux'
import { addToCart } from '../redux/actions/cartActions'


export const Home = ({ products, addToCart }) => {
  const createToast = () =>{
    const options = {
      html: "item added to your cart successfully",
      inDuration: 300,
      outDuration: 375,
      displyLength: 4000,
      classes: "rounded",
      completeCallback: () => {
        console.log("dismissed");
      }
    };
    M.toast(options);
  }
  const productsList = products.map(item => (
    <div className="card" key={item.id}>
      <div className="card-image">
        <img src={item.img} alt={item.title} />
        <span className="card-title">{item.title}</span>
        <span
          to="/"
          className="btn-floating halfway-fab waves-effect waves-light red"
          onClick={() => {
            addToCart(item)
            createToast()
          }}
        >
          <i className="material-icons">add</i>
        </span>
      </div>

      <div className="card-content">
        <p>{item.desc}</p>
        <p>
          <b>
            Price:
            {item.price}$
          </b>
        </p>
      </div>
    </div>
  ))

  return (
    <div className="container">
      <h3 className="center">Our items</h3>
      <div className="box">{productsList}</div>
    </div>
  )
}

const mapStateToProps = state => ({
  products: state.products,
})

const mapDispatchToProps = dispatch => ({
  addToCart: item => {
    dispatch(addToCart(item))
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
