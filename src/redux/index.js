import { createStore,compose,applyMiddleware } from 'redux'
import { rootReducer } from './reducers'

const middleWares = [];
 
if (process.env.NODE_ENV === `development`) {
  const { logger } = require(`redux-logger`);
 
  middleWares.push(logger);
}
 
export const store = compose(applyMiddleware(...middleWares))(createStore)(rootReducer);

