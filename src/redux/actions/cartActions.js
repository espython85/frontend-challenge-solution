import {
  ADD_QUANTITY,
  ADD_TO_CART,
  REMOVE_ITEM,
  SUB_QUANTITY,
} from './action-types/cart-actions'

export const addToCart = item => ({
  type: ADD_TO_CART,
  payload:item,
})

export const removeItem = id => ({
  type: REMOVE_ITEM,
  payload:id,
})

export const subtractQuantity = id => ({
  type: SUB_QUANTITY,
  payload:id,
})

export const addQuantity = id => ({
  type: ADD_QUANTITY,
  payload:id,
})
