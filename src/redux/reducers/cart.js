export default (state = { items: [] }, action) => {
  const cardItems = state.items;
  console.log("action", action);
  const addItems = (item)=>{
    
  }
  switch (action.type) {
    case "ADD_TO_CART": {
      let newItems = []
      const pastItem = [...cardItems].find (item=>(
       item.id === action.payload.id
      ))
      console.log('item vv',pastItem,'payload',action.payload)
      if(!pastItem){
        return {
          ...state,
          items: [...cardItems,{ ...action.payload, quantity: 1 } ],
        }

      }else if(pastItem){
        return {
          ...state,
          items: [...cardItems].map((item) => {
            if (item.id === action.payload.id) {
              return { ...item, quantity: item.quantity + 1 };
            }else {return item}
        })
      }
    }
  }
    case "REMOVE_ITEM": {
      return {
        ...state,
        items: [...cardItems].filter((item) => item.id !== action.payload),
      };
    }
    case "ADD_QUANTITY": {
      return {
        ...state,
        items: [...cardItems].map((item) => {
          if (item.id === action.payload) {
            return { ...item, quantity: item.quantity + 1 };
          }else {return item}
        }),
      };
    }
    case "SUB_QUANTITY": {
      return {
        ...state,
        items: [...cardItems].map((item) => {
          if (item.id === action.payload) {
            return { ...item, quantity: item.quantity > 0 ? item.quantity - 1 : item.quantity};
          }else {return item}
        }),
      };
    }
    default:
      return state;
  }
};
